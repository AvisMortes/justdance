import pygame

class Music:

    def __init__(self, name, music_style, sound_time, path):
        self.name = name
        self.music_style = music_style
        self.sound_time = sound_time
        self.path = path
        self.music = None
        self.is_playing = False

    def __str__(self):
        return self.name

    # Запуск трека
    def play(self):
        pygame.mixer.init()
        self.music = pygame.mixer.Sound(self.path)
        self.music.set_volume(0.05)
        self.music.play()
        return print("Текущий трек: {0} стиль: {1}".format(self.name, self.music_style))

    # Остановка трека
    def stop(self):
        self.music.stop()
