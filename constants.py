NAMES = ["Иван", "Максим", "Даниил", "Ренат", "Лиза",
         "Таня", "Лена", "Никита", "Ильмир", "Кирилл",
         "Роман", "Катя", "Настя", "Вероника", "Соня",
         "Аида", "Инсаф", "Артур", "Резеда", "Наташа", "Дамир"]
DANCE_STYLE = ["RnB", "HipHop", "Electrodance", "House", "Pop"]
MUSIC_STYLE = ["RnB", "Electrohouse", "Поп-музыка"]
# Привязка стилей к музыке
DANCE_WITH_MUSIC = {MUSIC_STYLE[0]: (DANCE_STYLE[0], DANCE_STYLE[1]),
                    MUSIC_STYLE[1]: (DANCE_STYLE[2], DANCE_STYLE[3]),
                    MUSIC_STYLE[2]: (DANCE_STYLE[4],)}

CONDITIONS = ["Пьет водку в баре", "Танцует"]

COMMANDS = ["Music", "Person"]