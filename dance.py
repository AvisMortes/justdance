class Dance:

    def __init__(self, name, body, legs, hands, head):
        self.name = name
        self.body = body
        self.legs = legs
        self.hands = hands
        self.head = head

    def __str__(self):
        return self.name

    # Заставляем персонажа танцевать
    def dancing(self, name="Ноунейм"):
        condition = '{0} двигает телом {1} , руки {2}, ноги {3}, головой {4}'.format(
            name, self.body, self.hands, self.legs, self.head)
        print(condition)
        return condition
