from dance import Dance
from people import People
from music import Music
from constants import *
import random
import numpy as np
import time
from threading import Thread


# Генератор персонажей и их танцевальных навыков
def generate_peoples(_dancing_skill):
    peoples = []
    for x in range(random.randint(1, 50)):
        dances = np.random.choice(_dancing_skill, size=random.randint(1, len(_dancing_skill)), replace=False)
        people = People(NAMES[random.randint(0, len(NAMES) - 1)], dances)
        peoples.append(people)
        #print(people.name, people.dance_style)
    return peoples


# Симуляция клуба
def dancing_in_da_club(track_list, peoples_list):
    for track in track_list:
        print('\n')
        track.play()
        track.is_playing = True
        print('\n')
        for people in peoples_list:
            dance = people.can_i_dance(track.music_style)
            if dance is not False:
                people.condition = dance.dancing(people)
            else:
                people.drink_vodka()
        time.sleep(track.sound_time)
        track.is_playing = False
        track.stop()


# Команды для консоли (Person, Music, Exit)
def console_command(command, people_list, track_list):
    # Команда Music выводить название трека
    if command == COMMANDS[0]:
        for track in track_list:
            if track.is_playing:
                return print('Играет трек:{0}'.format(track.name))
    # Команда Person выводит данные о действиях персонажа
    elif command == COMMANDS[1]:
        name = input("Введите имя:")
        for people in people_list:
            if people.name == name:
                print(people.condition)
        return
    else:
        return


if __name__ == "__main__":
    # Создаем танцевальные стили
    rnb = Dance(DANCE_STYLE[0], "вперед и назад", "в полу присяди", "согнуты", "вперед-назад")
    hiphop = Dance(DANCE_STYLE[1], "вперед и назад", "в полу присяди", "согнуты", "вперед-назад")
    ed = Dance(DANCE_STYLE[2], "вперед и назад", "двигаются в ритме",
               "делают круговые движения - вращения", "не шевелит")
    hs = Dance(DANCE_STYLE[3], "вперед и назад", "двигаются в ритме",
               "делают круговые движения - вращения", "не шевелит")
    pop = Dance(DANCE_STYLE[4], "плавно", "плавно движутся", "плавно движутся", "качает плавно")
    dancing_skill = (rnb, ed, hs, pop)
    # Записываем треки
    music_1 = Music("Mario", MUSIC_STYLE[0], 20, 'Music\Mario.wav')
    music_2 = Music("What is love", MUSIC_STYLE[1], 40, 'Music\What is love.wav')
    music_3 = Music("Tetris", MUSIC_STYLE[2], 30, 'Music\Tetris.wav')
    _track_list = (music_1, music_2, music_3)
    # генерируем персонажей и запускаем тусовку
    _peoples = generate_peoples(dancing_skill)
    thread1 = Thread(target=dancing_in_da_club, args=(_track_list, _peoples, ))
    thread1.start()
    command = None
    # Ждем консольных команд, при команде Exit заканчиваем тусу
    while command != "Exit":
        command = input()
        console_command(command, _peoples, _track_list)
    thread1.join()
    print("Наступило утро")
