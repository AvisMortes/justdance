from constants import DANCE_WITH_MUSIC,CONDITIONS


class People:

    def __init__(self, name, dance_style):
        self.name = name
        self.dance_style = dance_style
        self.condition = None
        
    def __str__(self):
        return self.name

    # Танцует ли персонаж под такую музыку?
    def can_i_dance(self, music_style):
        _dances = DANCE_WITH_MUSIC[music_style]
        for dance in self.dance_style:
            for dances in _dances:
                if dance.name == dances:
                    return dance
        return False

    # Пить или не пить
    def drink_vodka(self):
        self.condition = CONDITIONS[0]
        return print('{0} пьет водку в баре'.format(self.name))